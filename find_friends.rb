def find_friends(n, s, adj_matrix)
  #find friends of given coworker by his 'id'
  arr = [s]
  arr.each do |j|
    #check all rows
    n.times do |i|
      #Add friend to the array if it's not there already
      arr << i if !(adj_matrix[i][j] == 0 || arr.include?(i))
    end
  end
  # Substract ourself
  arr.count - 1
end

n = 3
s = 1

adj_matrix = [
  [0, 1, 0],
  [1, 0, 1],
  [0, 1, 0]
]

puts find_friends(n, s, adj_matrix)

=begin
adj_matrix = [
  [0, 0, 1, 0, 1, 0],
  [0, 0, 1, 0, 0, 1],
  [0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 1, 1],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0]
]
adj_matrix = [
  [0,1,0],
  [1,0,1],
  [0,1,0]
]
=end

