# Return the minimum time required to print m items
#
def min_time(x, y, m)
  #compare two rates
  x < y ? min = x : min = y
  # Equation for total time for different rates
  result = (x * y * m).ceil / (x + y)
  #we start from a single copy with min rate, thats why we must add min to final result
  (result + min).to_i
end

x = 1
y = 2
m = 5

puts(min_time(x, y, m))
