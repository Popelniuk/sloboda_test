def sums(n, arr)
  counter = 0
# Range for demo purpose i chose 1 up to 4.
  1.upto(arr.size + 1) do |i|
    # What we do here is generate combinations of arrays
    # incrementing a counter each time we find an array that contains ONLY numbers from array and its sum is equal to sum from input.
    counter += [*(1..n - arr.length + 1)].repeated_combination(i).select { |a| a.reduce(:+) == n && a.all? { |el| arr.include?(el) } }.size
    # Following 2 lines are for illustrative purposes only
    puts [*(1..n - arr.length + 1)].repeated_combination(i).select { |a| a.reduce(:+) == n && a.all? { |el| arr.include?(el) } }.inspect
    puts counter
  end
  counter
end

x = 10
y = 25
z = 15
sums(40, [x, y, z])
